PRODUCT_PACKAGES += \
    OnePlusCamera \
    OnePlusCameraService

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/system/etc/permissions/com.oneplus.camera.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.xml \
    $(LOCAL_PATH)/system/etc/permissions/com.oneplus.camera.pictureprocessing.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.pictureprocessing.xml \
    $(LOCAL_PATH)/system/etc/permissions/com.oneplus.camera.service.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/permissions/com.oneplus.camera.service.xml \
    $(LOCAL_PATH)/system/etc/sysconfig/hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM)/etc/sysconfig/hiddenapi-package-whitelist.xml \
    $(LOCAL_PATH)/system_ext/etc/permissions/privapp-permissions-oem-system_ext.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/permissions/privapp-permissions-oem-system_ext.xml \
    $(LOCAL_PATH)/system_ext/etc/sysconfig/hiddenapi-package-whitelist.xml:$(TARGET_COPY_OUT_SYSTEM_EXT)/etc/sysconfig/hiddenapi-package-whitelist.xml \
    $(LOCAL_PATH)/prebuilt/apps/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so:$(TARGET_COPY_OUT_SYSTEM_EXT)/priv-app/OnePlusCamera/lib/arm64/libsnpe_dsp_v66_domains_v2_skel.so
